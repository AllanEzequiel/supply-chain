(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".primaryCarousel").owlCarousel({
			loop: false,
			center: false ,
			nav: true,
			dots: true,
			margin: 20,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 15,
				},

				300: {
				items: 1,
				margin: 20,
				},
			
				1051: {
				items: 3,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

$(function() {
    var owl = $('.mobileCarousel'),
        owlOptions = {
			loop: false,
			center: false ,
			nav: true,
			margin: 20,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 15,
				},

				300: {
				items: 1,
				margin: 20,
				},
			}
        };

    if ( $(window).width() < 1051 ) {
        var owlActive = owl.owlCarousel(owlOptions);
    } else {
        owl.addClass('off');
    }

    $(window).resize(function() {
        if ( $(window).width() < 1051 ) {
            if ( $('.mobileCarousel').hasClass('off') ) {
                var owlActive = owl.owlCarousel(owlOptions);
                owl.removeClass('off');
            }
        } else {
            if ( !$('.mobileCarousel').hasClass('off') ) {
                owl.addClass('off').trigger('destroy.owl.carousel');
                owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
            }
        }
    });
});


(function ($,jQuery) {
	$('.mobile-menu').click(function(){
		$('body').toggleClass('mobileMenu_open');
	});

	$('.anchor').click(function(){
		$('body').removeClass('mobileMenu_open');
	});

	$(window).on("scroll", function() {
		if($(window).scrollTop() > 80) {
			$("body").addClass("scrolled");
		} else {
		   $("body").removeClass("scrolled");
		}
	});
	
	var position = $(window).scrollTop(); 
	$(window).scroll(function() {
	  var scroll = $(window).scrollTop();
	  if(scroll > position) {
		console.log('scrollDown');
		$("body").removeClass("menuvisible");
	  } else {
		console.log('scrollUp');
		$("body").addClass("menuvisible");
		$("body").removeClass("scrolled");
	  }
	  position = scroll;
	}); 


	$(document).ready(function () {
		$(document).on("scroll", onScroll);
		
		//smoothscroll
		$('.anchor').on('click', function (e) {
			e.preventDefault();
			$(document).off("scroll");
			
			$('a').each(function () {
				$(this).removeClass('active');
			})
			$(this).addClass('active');
		  
			var target = this.hash,
				menu = target;
			$target = $(target);
			$('html, body').stop().animate({
				'scrollTop': $target.offset().top+2
			}, 0, 'swing', function () {
				window.location.hash = target;
				$(document).on("scroll", onScroll);
			});
		});
	});
	
	function onScroll(event){
		var scrollPos = $(document).scrollTop();
		$('.nav-menu ul li .anchor').each(function () {
			var currLink = $(this);
			var refElement = $(currLink.attr("href"));
			if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
				$('.nav-menu ul li .anchor').removeClass("active");
				currLink.addClass("active");
			}
			else{
				currLink.removeClass("active");
			}
		});
	}

	
})(jQuery);


$(document).on('click', '.anchorMore', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 40
    }, 0);
});


